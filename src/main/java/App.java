import com.detectlanguage.DetectLanguage;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * <p>This app receives a text file with ONE word on each line, and returns an output text file with the JSON
 * computed by <a href="detectlanguage.com">detectlanguage.com</a> which detects what language the word is
 * and also has a confidence score</p>
 * <p>
 * <p>Downside: The JSON returned does not have the input variables in it, BUT it returns it in the same order as the input.</p>
 */
public class App {

    private static final String apiKey = "c72daec83436a60f1e1beb3c9aa8cc21"; //for adrian.pop@optimaize.com
    private static final String desktopPath = System.getProperty("user.home") + "/Desktop/";
    private static File output = new File(desktopPath + "output.txt");

    public static void main(String[] args) throws Exception {
        DetectLanguage.apiKey = apiKey;
        File input = new File(desktopPath + "input.txt");

        writeToOutputFile(getJsonResponse(computeRequest(input)));
    }


    /**
     * <p>Reads the input file and returns the query for the request needed for the API.</p>
     * <p>Doing this because I don't want to handle one request at a time, so I save time by giving the entire request at the same time.</p>
     * <p>This also dodges the maximum requests/day for a free account (5000) by 10-30 minutes, until their tracker detects that the number of requests was exceeded.</p>
     *
     * @param file Input file, currently a .txt
     * @return The computed request prepared for <a href="detectlanguage.com">detectlanguage.com</a> and then passed to {@link #getJsonResponse(String)}
     */
    private static String computeRequest(File file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));

        String line;
        String request = "";
        while ((line = br.readLine()) != null) {
            request += "q[]=" + URLEncoder.encode(line, "UTF-8") + "&";
        }

        br.close();
        return request;
    }

    /**
     * <p>Gets the JSON response from the API after the request was handled</p>
     *
     * @param request The request sent to <a href="detectlanguage.com">detectlanguage.com</a>
     * @return The JSON returned by <a href="detectlanguage.com">detectlanguage.com</a> that is passed on to {@link #writeToOutputFile(String)}
     */
    private static String getJsonResponse(String request) {

        String body = "";
        HttpURLConnection connection;
        try {
            String urlAndRequest = "http://ws.detectlanguage.com/0.2/detect?" + request + "key=" + apiKey;
            URL url = new URL(urlAndRequest);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            InputStream in = connection.getInputStream();
            String encoding = connection.getContentEncoding();
            encoding = encoding == null ? "UTF-8" : encoding;
            body = IOUtils.toString(in, encoding);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return body;
    }

    /**
     * <p>Creates the output .txt file that contains the JSON from the website, prettified using <a href='https://github.com/google/gson'>https://github.com/google/gson</a></p>
     *
     * @param json The contents that are going to be written in the file
     */
    private static void writeToOutputFile(String json) {

        JsonParser jsonParser = new JsonParser();
        JsonElement detections = ((JsonObject) ((JsonObject) jsonParser.parse(json)).get("data")).get("detections");
        JsonArray jsonArray = detections.getAsJsonArray();

        for (JsonElement jsonElement : jsonArray) {
            try {
                FileWriter fileWriter = new FileWriter(output, false);

                if (!jsonElement.toString().equals("[")) { //workaround the current bug present where an unknown parameter returns only a [
                    fileWriter.write(jsonElement.toString());
                }

                fileWriter.write("\n");
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
